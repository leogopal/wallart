<?php get_header(); ?>

	<div id="content" class="container">
		<div class="row">
			<div id="page">
				<?php if ( function_exists('yoast_breadcrumb') ) {
					yoast_breadcrumb('<div id="breadcrumbs">','</div>');
				} ?>

				<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); 
				$thumbnail = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' ); ?>
					
					
					<div class="row">
						
						<div class="productextras pull-right col-lg-6 col-md-12 col-xs-12">
							<div class="productpictures">
								<div class="svgpic">
									<div class="svgbackground"></div>
									<div class="svgbg">
										<img src="<?php if( get_field('blank_jpg') ) { the_field('blank_jpg'); } else { echo $thumbnail[0]; } ?>" />
									</div>
									<div class="thesvg">
										<a href="<?php echo $thumbnail[0]; ?>">
											<?php 
											if ( get_field('svg_file') ) {
												$url = wp_get_attachment_url(get_field('svg_file'));
												$uploads = wp_upload_dir();
												$file_path = str_replace( $uploads['baseurl'], $uploads['basedir'], $url );
												echo file_get_contents($file_path);
											}
											?>
										</a>
									</div>
								</div>
							</div><!-- product pictures -->

							<?php if ( get_field('svg_file') ) { ?>

							<div class="extras">
								<div class="productgallery">
									<?php get_template_part('woocommerce/single-product/product-thumbnails'); ?>
								</div>
							</div>

							<div class="palette">
								<div class="paletteheader">Wall Background <span>(Use this if you'd like to preview this design on a flat colour instead)</span></div>
								<div class="palettecolors clearfix">
									<?php 
										get_template_part('includes/content/colors'); 
										foreach($colors as $color) {
											echo '<a href="#" data-svglayer="svgbackground" data-color="'.$color['hex'].'" rel="tooltip" data-original-title="'.$color['color'].'"></a>';
										} ?>
									<a href="#" class="clearbackground">Remove background color</a>
								</div>
							</div><!-- palette -->

							<?php } ?>
							<p><i>Actual colours may vary from the colour on your screen due to monitor colour restrictions. The sizes indicated for the design are the overall area it will cover.</i></p>
							<?php the_content(); ?>
						</div><!-- product extras -->

						<div class="productinfo thecontent pull-left col-lg-6 col-md-12 col-xs-12">

							<div class="summary entry-summary">
								<?php
									/**
									 * woocommerce_single_product_summary hook
									 *
									 * @hooked woocommerce_template_single_title - 5
									 * @hooked woocommerce_template_single_price - 10
									 * @hooked woocommerce_template_single_excerpt - 20
									 * @hooked woocommerce_template_single_add_to_cart - 30
									 * @hooked woocommerce_template_single_meta - 40
									 * @hooked woocommerce_template_single_sharing - 50
									 */

									remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 30 );
									
									add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 10 );


									do_action( 'woocommerce_single_product_summary' );
								?>
							</div><!-- .summary -->
						</div><!-- productinfo -->

					</div>
				<?php endwhile; endif; ?>

				<div class="productpagination pull-right">
					<?php nmpza_product_pagination(); ?>
				</div>
				
				<div id="relatedstuff" class="row">
					<?php echo do_shortcode('[related_products posts_per_page="3"]'); ?>
				</div>

			</div><!-- page -->

			<?php get_sidebar(); ?>
		</div>
	</div><!-- content -->
	
<?php get_footer(); ?>
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title" id="myModalLabel">How it works</h4>
			</div>
			<div class="modal-body">
				<ul>
					<li>Once we receive the order and proof of payment, your order goes into production.</li>
					<li>You can order online or via email: sales@wallartstudios.com.</li>
					<li>Depending on how busy we are it can take 3-7 working days for us to make up.</li>
					<li>Each order is custom-made because you choose your own colours, sizes and direction facing.</li>
					<li>Once your order is complete, the courier collects and if you stay in a major city it will be delivered the next day. Outlying areas can take a few extra days but you can track your parcel with your waybill number which we email you once it has been collected.</li>
					<li>If you can’t find what you are looking for you can email us as we specialise in custom wall art designs, we need as much info as possible to work out the cost. Eg. Size and colours</li>
					<li>When looking at the image it is facing left, all images are defaulted to the left, if you choose right  the image will flip and that is how you will see it on your wall when facing it.</li>
					<li>There are 3 different size choices, when you click on them they will show you the price at the cart button, if you would like a different size, drop us an email and we will work out the cost for your size.</li>
					<li>Please make sure you have chosen your colours correctly and double check on your order form. We cannot be held responsible for incorrect colour choices.</li>
					<li>We include a detailed description of how to install your design with a colour picture of your design.</li>
					<li>We courier nation-wide and post internationally.</li>
					<li>We have installers in the Johannesburg region and in the greater Cape town areas. They work independently and need your address and the design to give you a price for the installation.</li>
				</ul>
			</div>
		</div>
	</div>
</div>